# Nautilus folders' sizes

## Nautilus extension to show folder sizes

Currently has a bug. At least one "unknown" item in every folder.

## Installation

```
mkdir ~/.local/share/nautilus-python/extensions/ -p
sudo apt install python-nautilus
git clone https://gitlab.com/losnappas/nautilus-folders-sizes ~/.local/share/nautilus-python/extensions/
chmod +x ~/.local/share/nautilus-python/extensions/NautilusFolderSize.py
```

~~sudo apt install libnautilus-extension-dev   (?)~~

## Learning materials:

https://pastebin.com/1djRPqna

https://mail.gnome.org/archives/nautilus-list/2013-July/msg00001.html

https://saravananthirumuruganathan.wordpress.com/2010/08/29/extending-nautilus-context-menus-using-nautilus-actions-scripts-and-python-extensions/

https://www.feeditout.com/python-column-provider-nautilus/

