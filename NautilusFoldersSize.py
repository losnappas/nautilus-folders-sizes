import os
import urllib
from subprocess import check_output
#import threading
#import time

from gi import require_version
require_version('Nautilus', '3.0')

from gi.repository import Nautilus, GObject

class NautilusFoldersSizeExtension(GObject.GObject, Nautilus.ColumnProvider, Nautilus.InfoProvider):
    def __init__(self):
        pass
    
    def get_columns(self):
        return Nautilus.Column(name="NautilusPython::fsize_column",
                               attribute="fsize",
                               label="fsize",
                               description="Get the size via du"),

    def cancel_update(self, provider, handle):
	pass


    def add_info(self, provider, handle, closure, file):
        filename = urllib.unquote(file.get_uri()[7:])
	du = check_output(["du", filename, "-sh"])
	size = du.split(None, 1)[0]

#	print 'and finally, size: %s' % size
	file.add_string_attribute('fsize', size)
	file.invalidate_extension_info()

	Nautilus.info_provider_update_complete_invoke(closure, provider, handle, Nautilus.OperationResult.COMPLETE)

        #return false to not run again
        return False

    def update_file_info_full(self, provider, handle, closure, file):
        if file.get_uri_scheme() != 'file':
            return
	GObject.idle_add(self.add_info, provider, handle, closure, file)
	return Nautilus.OperationResult.IN_PROGRESS

	#GObject.timeout_add(1, self.add_info, provider, handle, closure, file)
#	thread = threading.Thread(target=runner)
#	thread.daemon = True
#	thread.start()

